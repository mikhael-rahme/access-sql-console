﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer;
using System.Data.SqlClient;
using System.Diagnostics;

namespace AccessSQL
{
    class SQLMigration
    {
        private string sqlUser;
        private string sqlPass;
        private string dbName;
        private string dbIP;
        private string connString;
        private DataTable accTable;

        public SQLMigration(string sqlUsername, string sqlPassword, string databaseName, string databaseIP)
        {
            sqlUser = sqlUsername;
            sqlPass = sqlPassword;
            dbName = databaseName;
            dbIP = databaseIP;
            connString = @"Data Source=" + dbIP + ";Initial Catalog=" + dbName + ";Integrated Security=true;User ID=" + sqlUser + ";Password=" + sqlPass + ";";
            //connString = @"Data Source=" + sqlUser + ";Initial Catalog=" + dbName + ";Integrated Security=True;";
        }

        public void TableTransfer(DataTable accessTable)
        {
            accTable = accessTable;
            string cmd = "CREATE TABLE [dbo].[" + accTable.TableName + "](";

            for (int i = 0; i < accTable.Columns.Count; i++)
            {
                cmd += "[" + accTable.Columns[i].ColumnName.ToString() + "] [nchar](255), ";
            }
            cmd = cmd.Remove(cmd.Length - 2);
            cmd += ");";

            SqlCommand sqlTbl = new SqlCommand(cmd, new SqlConnection(connString));
            sqlTbl.Connection.Open();
            sqlTbl.ExecuteNonQuery();
            sqlTbl.Connection.Close();

            try
            {
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connString);

                sqlBulkCopy.DestinationTableName = accTable.TableName;

                foreach (var column in accTable.Columns)
                    sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                sqlBulkCopy.WriteToServer(accTable);

                Console.Write("Table transferred successfully!");
            }
            catch
            {
                Console.Write("Error migrating table!");
            }
        }

        public void DatabaseTransfer(List<DataTable> accessTables)
        {
            for (int j = 0; j < accessTables.Count; j++)
            {
                accTable = accessTables[j];
                string cmd = "CREATE TABLE [dbo].[" + accTable.TableName + "](";

                for (int i = 0; i < accTable.Columns.Count; i++)
                {
                    cmd += "[" + accTable.Columns[i].ColumnName.ToString() + "] [varchar](max), ";
                }
                cmd = cmd.Remove(cmd.Length - 2);
                cmd += ");";

                SqlCommand sqlTbl = new SqlCommand(cmd, new SqlConnection(connString));
                sqlTbl.Connection.Open();
                sqlTbl.ExecuteNonQuery();
                sqlTbl.Connection.Close();

                try
                {
                    SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connString);

                    sqlBulkCopy.DestinationTableName = accTable.TableName;

                    foreach (var column in accTable.Columns)
                        sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                    sqlBulkCopy.WriteToServer(accTable);
                }
                catch (Exception ex)
                {
                    Console.Write("Error migrating table: " + ex);
                }
            }           
        }
    }
}
