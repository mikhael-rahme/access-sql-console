﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccessSQL;
using System.Data;

namespace AccessSQLConsole
{
    class Program
    {
        public static List<DataTable> transferData;

        public static List<DataTable> TransferData
        {
            get
            {
                return transferData;
            }
            set
            {
                transferData = value;
            }
        }

        static void Main(string[] args)
        {
            ConsoleUI program = new ConsoleUI();

            program.AccessLogin();
            program.LoadTableNames();
            TransferData = program.CopyTables();
            program.SqlLogin();
            program.TransferTables(TransferData);
        }
    }
}
