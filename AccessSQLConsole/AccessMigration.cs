﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer;
using System.Data.SqlClient;
using System.Diagnostics;

namespace AccessSQL
{
    public class AccessMigration
    {
        private string accessPath;
        private List<string> accessTitle;
        private string accessPass;
        private string accessString;
        private string compName;

        public AccessMigration(string accessFilePath, string accessPassword, string computerName)
        {
            accessPath = accessFilePath;
            accessPass = accessPassword;
            accessString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                            "Data Source=" + accessPath + ";" +
                            "Persist Security Info=True;" +
                            "Jet OLEDB:Database Password=" + accessPass + ";";
            compName = computerName;
        }

        public List<string> TableNames()
        {
            OleDbConnection oleDbConnection = new OleDbConnection();
            oleDbConnection.ConnectionString = accessString;
            oleDbConnection.Open();

            //restricting titles returned to user tables only
            DataTable tableNames = null;
            string[] restrictions = new string[4];
            restrictions[3] = "Table";
            tableNames = oleDbConnection.GetSchema("Tables", restrictions);

            var names = from row in tableNames.AsEnumerable()
                             select row.Field<string>("TABLE_NAME");

            var namesArray = names.ToList<string>();

            accessTitle = namesArray;

            //names returned as array
            return namesArray;
        }

        public DataTable TableCopy( string titleSelection )
        {
            OleDbConnection accessConnection = new OleDbConnection();
            accessConnection.ConnectionString = accessString;
            accessConnection.Open();
            //debug message
            Console.WriteLine("CONNECTION: SUCCESS!");

            //reading & copying table into new DataTable
            OleDbCommand cmd = accessConnection.CreateCommand();
            cmd.CommandText = "SELECT * FROM `" + titleSelection + "`";
            OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            //debug message
            Console.WriteLine("ACCESS TABLE COPIED SUCCESSFULLY!");

            DataTable dataTable = new DataTable();
            dataTable.TableName = titleSelection;
            dataTable.Load(reader);

            //Adding source computer column to DataTable, unless it exists already.
            DataColumnCollection columns = dataTable.Columns;
            if (!columns.Contains("Source Computer"))
            {
                DataColumn srcComputer = new DataColumn("Source Computer", typeof(String));
                if (compName == "")
                {
                    srcComputer.DefaultValue = Environment.MachineName;
                }
                else
                {
                    srcComputer.DefaultValue = compName;
                }
                dataTable.Columns.Add(srcComputer);

                return dataTable;
            }      
            else
            {
                return dataTable;
            }
        }

        public List<DataTable> DatabaseCopy(List<string> titleSelections)
        {
            OleDbConnection accessConnection = new OleDbConnection();
            accessConnection.ConnectionString = accessString;
            accessConnection.Open();
            //debug message
            Console.WriteLine("CONNECTION: SUCCESS!");

            List<DataTable> dataTables = new List<DataTable>();

            for (int i = 0; i < titleSelections.Count; i++)
            {
                //reading & copying table into new DataTable
                OleDbCommand cmd = accessConnection.CreateCommand();
                cmd.CommandText = "SELECT * FROM `" + titleSelections[i] + "`";
                OleDbDataReader reader = cmd.ExecuteReader();

                DataTable dataTable = new DataTable();
                dataTable.TableName = titleSelections[i];
                dataTable.Load(reader);

                //Adding source computer column to DataTable, unless it exists already.
                DataColumnCollection columns = dataTable.Columns;
                if (!columns.Contains("Source Computer"))
                {
                    DataColumn srcComputer = new DataColumn("Source Computer", typeof(String));
                    if (compName == "")
                    {
                        srcComputer.DefaultValue = Environment.MachineName;
                    }
                    else
                    {
                        srcComputer.DefaultValue = compName;
                    }
                    dataTable.Columns.Add(srcComputer);

                    dataTables.Add(dataTable);
                }
                else
                {
                    dataTables.Add(dataTable);
                }
            }
            return dataTables;
        }
    }
}
