﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccessSQL;

namespace AccessSQLConsole
{
    class ConsoleUI
    {
        private AccessMigration accessConnection;
        private static SQLMigration sqlConnection;
        private List<string> accessTitles;
        private List<int> transferIndexes;
        private int transferIndex;
        private string accessPath;
        private string accessPass;
        private string compName;
        private string user;
        private string pass;
        private string IP;
        private string databaseName;
        
        public void AccessLogin()
        {
            int checkpoint = 0;
            string response;

            Console.WriteLine("----- ACCESS-TO-SQL DATABASE TRANSFER -----");
            compName = Environment.MachineName;
            Console.WriteLine("         logged on as: " + compName);
            Console.WriteLine("Would you like to change login name? (y/n)");
            response = Console.ReadLine();
            while (checkpoint != 1)
            {
                if (response == "y")
                {
                    Console.Write("NEW LOGIN NAME: ");
                    compName = Console.ReadLine();
                    Console.Write("logged on as: " + compName + "\n");
                    checkpoint++;
                }
                else if (response == "n")
                {
                    break; 
                }
                else
                {
                    Console.WriteLine("Would you like to change login name? (y/n)");
                    response = Console.ReadLine();
                }
            }
            Console.Write("ACCESS PATH: ");
            accessPath = Console.ReadLine();
            Console.Write("ACCESS PASSWORD: ");
            accessPass = Console.ReadLine();
            accessConnection = new AccessMigration(accessPath, accessPass, compName);
        }

        public void LoadTableNames()
        {
            try
            {
                accessTitles = accessConnection.TableNames();
                Console.WriteLine("MSG: Table names located successfully!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex);
            }
        }

        public List<DataTable> CopyTables()
        {
            List<DataTable> transferData = new List<DataTable>();
            transferIndexes = new List<int>();
            Console.WriteLine("TABLE(S) FOR TRANSFER:");
            for (int i = 0; i < accessTitles.Count; i++)
            {
                Console.WriteLine((i + 1) + ". " + accessTitles[i]);
            }
            Console.Write("ENTER INDEX(ES) FOR TRANSFER (-1 TO EXIT): \n");
            while (transferIndex != -1)
            {
                transferIndex = Convert.ToInt32(Console.ReadLine());
                if (transferIndex > accessTitles.Count || transferIndex == 0 || transferIndex < -1)
                {
                    Console.WriteLine("PLEASE ENTER NUMBER WITHIN LIST RANGE, OR -1 TO FINISH:");
                }
                else if (transferIndex == -1)
                {
                    continue;
                }
                else
                {
                    transferIndexes.Add(transferIndex);
                }
            }

            List<string> transferTitles = new List<string>();
            for (int j = 0; j < transferIndexes.Count; j++)
            {
                transferTitles.Add(accessTitles[(transferIndexes[j])-1]);
            }

            transferData = accessConnection.DatabaseCopy(transferTitles);

            return transferData;
        }

        public void SqlLogin()
        {
            Console.Write("SQL USERNAME: ");
            user = Console.ReadLine();

            Console.Write("PASSWORD? (y/n):");
            string checker1 = Console.ReadLine();
            int checknum1 = 0;
            while (checknum1 != 1)
            {
                if (checker1 == "y")
                {
                    Console.Write("PASSWORD: ");
                    pass = Console.ReadLine();
                    checknum1++;
                }
                else if (checker1 == "n")
                {
                    Console.WriteLine("BYPASSING PASSWORD . . .");
                    checknum1++;
                }
                else
                {
                    Console.Write("PLEASE ENTER VALID CHARACTER (y/n): ");
                    checker1 = Console.ReadLine();
                }
            }
            
            Console.Write("DB NAME: ");
            databaseName = Console.ReadLine();

            Console.Write("DB IP? (y/n): ");
            string checker2 = Console.ReadLine();
            int checknum2 = 0;
            while (checknum2 != 1)
            {
                if (checker2 == "y")
                {
                    Console.Write("IP: ");
                    IP = Console.ReadLine();
                    checknum2++;
                }
                else if (checker2 == "n")
                {
                    Console.WriteLine("BYPASSING IP . . .");
                    checknum2++;
                }
                else
                {
                    Console.Write("PLEASE ENTER VALID CHARACTER (y/n): ");
                    checker1 = Console.ReadLine();
                }
            }
        }

        public void TransferTables(List<DataTable> data)
        {
            try
            {
                sqlConnection = new SQLMigration(user, pass, databaseName, IP);
                sqlConnection.DatabaseTransfer(data);
                Console.Write("TRANSFER SUCCESS!");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.Write("ERR" + ex);
                Console.ReadLine();
            }
        }
    }
}
